﻿using AltSourceBankLedger.Enum;
using AltSourceBankLedger.Exceptions;
using AltSourceBankLedger.Models;
using AltSourceBankLedger.Services.Implementations;
using AltSourceBankLedger.Services.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace AltSourceBankLedgerTests.Services
{
    [TestClass]
    public class AccountServiceTests
    {
        private IAccountService _accountService;

        [TestInitialize]
        public void Initialize()
        {
            _accountService = new AccountService();
        }

        [TestMethod]
        public void Initialize_CreateAccount_CanLogin()
        {
            string email = "mike@hello.com";
            string password = "12345";

            var accountCreationResult =_accountService.CreateAccount(email, password);

            Assert.IsTrue(accountCreationResult.Equals(AccountCreationFeedback.Success));

            var result = _accountService.SignIn(email, password);
        }

        [TestMethod]
        public void Initialize_AttemptToCreateAccountThatAlreadyExists_ReturnsError()
        {
            string email = "mike@hello.com";
            string password = "12345";

            var accountCreationResult = _accountService.CreateAccount(email, password);

            var secondAccountCreationResult = _accountService.CreateAccount(email, password);

            Assert.IsTrue(secondAccountCreationResult.Equals(AccountCreationFeedback.AccountAlreadyExists));
        }

        // create account balance at 100
        [TestMethod]
        public void Initialize_CreateAccount_BalanceIs100Dollars()
        {
            string email = "test@email.com";
            string password = "12345";

            var newUser = new User(email, password);

            var accountCreationResult = _accountService.CreateAccount(email, password);

            int balance = _accountService.GetAccountBalance(newUser);

            Assert.IsTrue(balance == 100);
        }

        [TestMethod]
        public void Initialize_CreateAccountAndDeposit100_BalanceIs200()
        {
            User newUser = CreateAccount();

            _accountService.DepositToAccount(newUser,100);
            int balance = _accountService.GetAccountBalance(newUser);

            Assert.IsTrue(balance == 200);
        }

        [TestMethod]
        public void Initialize_CreateAccountAndWithdraw40_BalanceIs60()
        {
            User newUser = CreateAccount();

            _accountService.WithdrawFromAccount(newUser, 40);
            int balance = _accountService.GetAccountBalance(newUser);

            Assert.IsTrue(balance == 60);
        }

        [TestMethod]
        public void Initialize_CreateAccountAndPerformFourTransactions_HistoryReturnsAListOfFourTransactions()
        {
            User newUser = CreateAccount();

            _accountService.DepositToAccount(newUser, 40);
            _accountService.DepositToAccount(newUser, 40);
            _accountService.DepositToAccount(newUser, 40);
            _accountService.DepositToAccount(newUser, 40);
            List<Transaction> transactions = _accountService.GetAccountTransactionHistory(newUser);

            Assert.IsTrue(transactions.Count == 4);
        }

        [TestMethod]
        public void Initialize_CreateAccountAndWithdrawMoreThanBalance_ThrowsException()
        {
            User newUser = CreateAccount();
            bool caughtException = false;
            try
            {

                _accountService.WithdrawFromAccount(newUser, 110);
            }
            catch (AttemptingToWithdrawMoreThanAccountBalanceException e)
            {
                caughtException = true;
            }

            Assert.IsTrue(caughtException);
        }

        [TestMethod]
        public void Initialize_CreateAccountAndLogin_ReturnsUserAuthKey()
        {
            User newUser = CreateAccount();
            
            Tuple<bool,string> successAndKey = _accountService.SignIn(newUser.Email, newUser.Password);

            Assert.IsTrue(successAndKey.Item2 != "");
        }

        public User CreateAccount()
        {
            string email = "test@email.com";
            string password = "12345";

            var newUser = new User(email, password);

            var accountCreationResult = _accountService.CreateAccount(email, password);

            return newUser;
        }

    }
}
