const ledgerAPIUrl = 'https://localhost:44307/api/account';
window.ledgerAuthCookie = '';

// events
const loginEvent = 'login-event';
const logoutEvent = 'logout-event';

function convertAspNetCoreModelErrorToReadableString(errorObject)
{
    let humanReadableError = "";
    for(var key in errorObject)
    {
        humanReadableError = humanReadableError + errorObject[key].join('\n') + '\n';
    }
    return humanReadableError;
}

// Create Account

Vue.component('create-account-component', {
    data: function () {
        return {
            email: '',
            password: ''
        }
    },
    methods: {
        createAccount: function(event) {
            var self = this;
            axios(
                {
                    method: 'post',
                    headers: {
                        'Access-Control-Allow-Origin': '*',
                    },
                    url: ledgerAPIUrl,
                    data: {
                        email: this.email,
                        password: this.password
                    }
                }
            )
            .then(function (response) {
                alert(`Account successfully created for ${self.email}.`);
                self.resetForm(self);
            })
            .catch(function(error){
                console.log(error.response.data);
                let humanReadableError = "Create account failed due to these reasons: \n";
                const errorObject = error.response.data;

                if(error.response.status == 400)
                {
                    for(var key in errorObject)
                    {
                        humanReadableError = humanReadableError + errorObject[key].join('\n') + '\n';
                    }
                }
                else if(error.response.status == 409)
                {
                    humanReadableError = humanReadableError + errorObject;
                }
                else
                {
                    humanReadableError = "An error occurred.";
                }
                
                alert(humanReadableError);
                self.resetForm(self);
            });
        },
        resetForm: (self) => {
            self.email = "";
            self.password = "";
        }
    },
    template: 
            `<div class="create-account-form">
                <h2>Create Account</h2>
                <input v-model="email" type="text" placeholder="Enter your email">
                <input v-model="password" type="password" placeholder="Enter your password">

                <button v-on:click="createAccount">Create Account</button>
            </div>`
});


// Login

Vue.component('login-component', {
        data: function () {
            return {
                email: '',
                password: '',
                isLoggedIn: false
            };
        },
        computed: {
            welcomeMessage : function() {
                return `You are logged in as : ${this.email}`;
            }
        },
        methods: {
            login: function(event) {
                var self = this;
                axios(
                    {
                        method: 'post',
                        headers: {
                            'Access-Control-Allow-Origin': '*',
                        },
                        url: ledgerAPIUrl + "/login",
                        data: {
                            email: this.email,
                            password: this.password
                        }
                    }
                )
                .then(function (response) {
                    self.isLoggedIn = true;
                    self.$emit(loginEvent);
                    window.ledgerAuthCookie = response.data;
                })
                .catch(function(error){
                    alert('There was an error logging into your account.');
                });
            },
            logout: function(event) {
                var self = this;
                axios(
                    {
                        method: 'post',
                        headers: {
                            'Access-Control-Allow-Origin': '*',
                            'UserAuthKey' : window.ledgerAuthCookie
                        },
                        url: ledgerAPIUrl + "/logout"
                    }
                )
                .then(function (response) {
                    window.ledgerAuthCookie = "";
                    self.email = "";
                    self.password = "";
                    self.isLoggedIn = false;
                    self.$emit(logoutEvent);
                })
                .catch(function(error){
                    console.log(error);
                });
            },
        },
        template: 
            `<div class="login-form">
                <template v-if="!isLoggedIn">
                    <h2>Login</h2>
                    <input v-model="email" type="text" placeholder="Enter your email">
                    <input v-model="password" type="password" placeholder="Enter your password">
                
                    <button v-on:click="login">Login</button>
                </template>
                
                <template v-if="isLoggedIn">
                    <h2>{{ welcomeMessage }}</h2>
                    <button v-on:click="logout">Logout</button>
                </template>

               
            </div>`
    });

// Transaction Component

Vue.component('transactions-component', {
    data: function () {
        return {
            depositAmount: '',
            withdrawalAmount: '',
            status: '',
            transactions: []
        };
    },
    methods: {
        deposit: function(event) {
            var self = this;
            axios(
                {
                    method: 'post',
                    headers: {
                        'Access-Control-Allow-Origin': '*',
                        'UserAuthKey' : window.ledgerAuthCookie
                    },
                    url: ledgerAPIUrl + "/deposit",
                    data: {
                        "DollarAmountDepositedOrWithdrawn" : this.depositAmount
                    }
                }
            )
            .then(function (response) {
                self.status = response.data;
            })
            .catch(function(error){
                self.status = convertAspNetCoreModelErrorToReadableString(error.response.data);
            });
        },
        withdraw: function(event) {
            var self = this;
            axios(
                {
                    method: 'post',
                    headers: {
                        'Access-Control-Allow-Origin': '*',
                        'UserAuthKey' : window.ledgerAuthCookie
                    },
                    url: ledgerAPIUrl + "/withdraw",
                    data: {
                        "DollarAmountDepositedOrWithdrawn" : this.withdrawalAmount
                    }
                }
            )
            .then(function (response) {
                self.status = response.data;
            })
            .catch(function(error){
                self.status = convertAspNetCoreModelErrorToReadableString(error.response.data);
            });
        },
        checkBalance: function(event) {
            var self = this;
            axios(
                {
                    method: 'get',
                    headers: {
                        'Access-Control-Allow-Origin': '*',
                        'UserAuthKey' : window.ledgerAuthCookie
                    },
                    url: ledgerAPIUrl + "/balance"
                }
            )
            .then(function (response) {
                self.status = `Your balance is : ${response.data}`;
            })
            .catch(function(error){
                self.status = error.response.data;
            });
        },
        getTransactionHistory: function(event) {
            var self = this;
            axios(
                {
                    method: 'get',
                    headers: {
                        'Access-Control-Allow-Origin': '*',
                        'UserAuthKey' : window.ledgerAuthCookie
                    },
                    url: ledgerAPIUrl + "/transaction-history"
                }
            )
            .then(function (response) {
                const moreReadableTransactions = response.data.map((transaction) => {
                    return {
                        type : self.convertIntToTransactionType(transaction.type),
                        date : transaction.timeOfTransaction,
                        amount : transaction.dollarAmountDepositedOrWithdrawn
                    };
                });
                self.transactions = moreReadableTransactions;
            })
            .catch(function(error){
                self.status = error.response.data;
            });
        },
        convertIntToTransactionType: (transactionType) => {
            if(transactionType == 0)
                return 'Checked Balance';
            else if(transactionType  == 1)
                return 'Deposit';
            else
                return 'Withdrawal';
        }
    },
    template: 
        `<div class="transactions-component">
            <h2>Perform your transactions</h2>
            <div>
                <input type="number" min="1" v-model="depositAmount" placeholder="Deposit Amount">
                <button :disabled="!depositAmount"  v-on:click="deposit">Deposit</button>
            </div>
            <div>
                <input type="number" v-model="withdrawalAmount" placeholder="Withdrawal Amount">
                <button :disabled="!withdrawalAmount"  v-on:click="withdraw">Withdraw</button>
            </div>

            <div>
                <span>Check your balance :</span>
                <button  v-on:click="checkBalance">Balance</button>
            </div>

            <div> {{ status }} </div>

            <h2> Transaction History </h2>
            <button v-on:click="getTransactionHistory">Get History</button>

            <table>
                <tr>
                    <th>Date</th>
                    <th>Amount Deposited/Withdrawn</th>
                    <th>Type</th>
                </tr>
                <tr v-for="transaction in transactions">
                    <th>{{ transaction.date }}</th>
                    <th>{{ transaction.amount }}</th>
                    <th>{{ transaction.type }}</th>
                </tr>
            </table>            
        </div>`
});


var app = new Vue({
    el: '#ledger-app',
    data: {
        isLoggedIn : false
    } 
});