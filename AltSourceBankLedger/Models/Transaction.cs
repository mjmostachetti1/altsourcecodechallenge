﻿using AltSourceBankLedger.Enum;
using AltSourceBankLedger.ModelValidators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AltSourceBankLedger.Models
{
    public class Transaction
    {
        public Transaction(TransactionType type, int dollarAmount)
        {
            Type = type;
            TimeOfTransaction = DateTime.UtcNow;
            DollarAmountDepositedOrWithdrawn = dollarAmount;
        }
        public TransactionType Type {get;set;}
        public DateTime TimeOfTransaction { get; set; }
        [PositiveNumberAttribute]
        public int DollarAmountDepositedOrWithdrawn { get; set; }
    }
}
