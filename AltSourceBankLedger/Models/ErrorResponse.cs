﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AltSourceBankLedger.Models
{
    public class ErrorResponse
    {
        public ErrorResponse(List<string> errors)
        {
            Errors = new List<string>();
            foreach (var error in errors)
            {
                Errors.Add(error);
            }
        }

        public List<string> Errors { get; set; }
    }
}
