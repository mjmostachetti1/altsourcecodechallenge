﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AltSourceBankLedger.Models
{
    public class Account
    {
        public List<Transaction> TransactionHistory { get; set; }
        private const int InitalAccountAmount = 100;
        public Account(User user)
        {
            User = user;
            // Assume all accounts start with a base of $100
            Balance = InitalAccountAmount;
            TransactionHistory = new List<Transaction>();
        }

        public User User { get; private set; }
        public int Balance { get; set; }
    }
}
