﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AltSourceBankLedger.Enum
{
    public enum TransactionType
    {
        BalanceCheck,
        Deposit,
        Withdrawal
    }
}
