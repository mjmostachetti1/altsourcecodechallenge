﻿using AltSourceBankLedger.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AltSourceBankLedger.Services.Interfaces
{
    public interface IAccountContextService
    {
        User GetLoggedInUser();
        void Set(User user);
    }
}
