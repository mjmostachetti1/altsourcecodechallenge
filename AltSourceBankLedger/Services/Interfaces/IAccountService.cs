﻿using AltSourceBankLedger.Enum;
using AltSourceBankLedger.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AltSourceBankLedger.Services.Interfaces
{
    public interface IAccountService
    {
        Tuple<bool, string> SignIn(string email, string password);
        bool SignOut(User loggedInUser);
        AccountCreationFeedback CreateAccount(string email, string password);
        List<User> GetRegisteredUsers();
        int GetAccountBalance(User user);
        List<Transaction> GetAccountTransactionHistory(User user);
        bool WithdrawFromAccount(User user, int amountToWithdraw);
        bool DepositToAccount(User user, int depositAmount);
    }
}
