﻿using AltSourceBankLedger.Enum;
using AltSourceBankLedger.Exceptions;
using AltSourceBankLedger.Models;
using AltSourceBankLedger.Services.Interfaces;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace AltSourceBankLedger.Services.Implementations
{
    public class AccountService : IAccountService
    {
        public List<User> Users { get; set; } = new List<User>();
        public List<Account> Accounts { get; set; } = new List<Account>();

        public AccountService()
        {
            
        }

        public Tuple<bool,string> SignIn(string email, string password)
        {
            User existingUser = Users.Where(user => user.Email == email && user.Password == password).FirstOrDefault();

            if (existingUser != null)
            {
                var hmac = new HMACSHA256();
                var newSecretKey = Convert.ToBase64String(hmac.Key);
                existingUser.SecretKey = newSecretKey;
                existingUser.IsLoggedIn = true;
                return new Tuple<bool,string>(true, newSecretKey);
            }
            else
            {
                return new Tuple<bool,string>(false,"");
            }
        }

        public bool SignOut(User loggedInUser)
        {
            User userToLogout = Users.Where(user => user.SecretKey == loggedInUser.SecretKey).FirstOrDefault();

            if (userToLogout != null)
            {
                userToLogout.SecretKey = "";
                userToLogout.IsLoggedIn = false;
                return true;
            }
            else
            {
                return false;
            }
        }

        public AccountCreationFeedback CreateAccount(string email, string password)
        {
            var userAlreadyExists = Users.FirstOrDefault(user => user.Email == email);

            if (userAlreadyExists != null)
            {
                return AccountCreationFeedback.AccountAlreadyExists;
            }
            else
            {
                var newUser = new User(email, password);
                Users.Add(newUser);

                var newAccount = new Account(newUser);
                Accounts.Add(newAccount);

                return AccountCreationFeedback.Success;
            }
        }

        public List<User> GetRegisteredUsers()
        {
            return Users;
        }

        public int GetAccountBalance(User user)
        {
            var accountOfUser = Accounts.Where(account => user.Email == account.User.Email).FirstOrDefault();

            if (accountOfUser != null)
            {
                var getBalanceTransaction = new Transaction(TransactionType.BalanceCheck, 0);
                accountOfUser.TransactionHistory.Add(getBalanceTransaction);
                return accountOfUser.Balance;
            }
            else
                throw new CouldNotFindAccountForSpecifiedUser();
        }

        public List<Transaction> GetAccountTransactionHistory(User user)
        {
            var accountOfUser = Accounts.Where(account => user.Email == account.User.Email).FirstOrDefault();

            if (accountOfUser != null)
                return accountOfUser.TransactionHistory;
            else
                throw new CouldNotFindAccountForSpecifiedUser();
        }

        public bool WithdrawFromAccount(User user, int amountToWithdraw)
        {
            Account accountOfUser = Accounts.Where(account => user.Email == account.User.Email).FirstOrDefault();

            if (accountOfUser != null)
            {
                if (accountOfUser.Balance >= amountToWithdraw)
                {
                    accountOfUser.Balance = accountOfUser.Balance - amountToWithdraw;
                    var withdrawalTransaction = new Transaction(TransactionType.Withdrawal, amountToWithdraw);
                    accountOfUser.TransactionHistory.Add(withdrawalTransaction);
                    return true;
                }
                else
                {
                    throw new AttemptingToWithdrawMoreThanAccountBalanceException();
                }
            }
            else
                return false;
        }

        public bool DepositToAccount(User user, int depositAmount)
        {
            var accountOfUser = Accounts.Where(account => user.Email == account.User.Email).FirstOrDefault();

            if (accountOfUser != null)
            {
                accountOfUser.Balance = accountOfUser.Balance + depositAmount;
                var withdrawalTransaction = new Transaction(TransactionType.Deposit, depositAmount);
                accountOfUser.TransactionHistory.Add(withdrawalTransaction);
                return true;
            }
            else
                return false;
        }
    }
}
