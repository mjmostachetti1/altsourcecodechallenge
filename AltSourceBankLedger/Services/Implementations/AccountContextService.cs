﻿using AltSourceBankLedger.Models;
using AltSourceBankLedger.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AltSourceBankLedger.Services.Implementations
{
    public class AccountContextService : IAccountContextService
    {
        HttpContext context;
        const string userKey = "User";

        public AccountContextService(IHttpContextAccessor httpContentAccessor)
        {
            this.context = httpContentAccessor.HttpContext;
        }

        public User GetLoggedInUser()
        {
            object user;
            context.Items.TryGetValue(userKey, out user);
            return (User)user;
        }

        public void Set(User user)
        {
            context.Items.Remove(userKey);
            context.Items.Add(userKey, user);
        }
    }
}
