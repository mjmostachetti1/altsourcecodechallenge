﻿using AltSourceBankLedger.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace AltSourceBankLedger.Filter
{
    public class UserAuthorisationFilter : ActionFilterAttribute
    {
        private readonly IAccountService _accountService;
        private readonly IAccountContextService _accountContextService;

        public UserAuthorisationFilter(
            IAccountService accountService,
            IAccountContextService accountContextService)
        {
            _accountService = accountService;
            _accountContextService = accountContextService;
        }

        public override void OnActionExecuting(ActionExecutingContext executingContext)
        {
            try
            {
                var authKey = executingContext.HttpContext.Request.Headers.FirstOrDefault(x => x.Key.ToLower() == "UserAuthKey".ToLower());
                //If the AuthenticationPin isn't in the headers, this will error out and cause an exception
                var userAuthKey = authKey.Value[0];

                if (String.IsNullOrWhiteSpace(userAuthKey))
                {
                    executingContext.HttpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                    executingContext.Result = new JsonResult("Sorry, you are not authorized");
                }

                var authorisedUser = _accountService
                    .GetRegisteredUsers()
                    .Where(user => user.SecretKey == userAuthKey)
                    .FirstOrDefault();

                if (authorisedUser != null)
                {
                    _accountContextService.Set(authorisedUser);
                }
                else
                {
                    executingContext.HttpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                    executingContext.Result = new JsonResult("Sorry, you are not authorized");
                }
            }
            catch (Exception e)
            {
                executingContext.HttpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                executingContext.Result = new JsonResult("Sorry, you are not authorized");
            }
        }
    }
}
