﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AltSourceBankLedger.ModelValidators
{
    public class PositiveNumberAttribute : ValidationAttribute
    {


        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            int intVal;
            if (int.TryParse(value.ToString(), out intVal))
            {
                if (intVal == 0)
                    return new ValidationResult("You cannot deposit/withdraw 0 dollars");

                if (intVal < 0)
                    return new ValidationResult("You cannot deposit/withdraw negative integers");
            }
            return ValidationResult.Success;
        }
    }
}
