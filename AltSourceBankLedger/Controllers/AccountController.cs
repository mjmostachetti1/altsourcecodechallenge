﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AltSourceBankLedger.Exceptions;
using AltSourceBankLedger.Filter;
using AltSourceBankLedger.Models;
using AltSourceBankLedger.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AltSourceBankLedger.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly IAccountService _accountService;
        private readonly IAccountContextService _accountContextService;

        public AccountController(
            IAccountService accountService,
            IAccountContextService accountContextService)
        {
            _accountService = accountService;
            _accountContextService = accountContextService;
        }

        [HttpGet("transaction-history")]
        [ServiceFilter(typeof(UserAuthorisationFilter))]
        public ActionResult History()
        {
            try
            {
                User currentUser = _accountContextService.GetLoggedInUser();
                List<Transaction> transactionHistory = _accountService.GetAccountTransactionHistory(currentUser);
                return Ok(transactionHistory);
            }
            catch (CouldNotFindAccountForSpecifiedUser e)
            {
                // We should never get here unless something went wrong with our datastore!
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpGet("balance")]
        [ServiceFilter(typeof(UserAuthorisationFilter))]
        public ActionResult Balance()
        {
            try
            {
                User currentUser = _accountContextService.GetLoggedInUser();
                int userBalance = _accountService.GetAccountBalance(currentUser);
                return Ok(userBalance);
            }
            catch (CouldNotFindAccountForSpecifiedUser e)
            {
                // We should never get here unless something went wrong with our datastore!
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPost("logout")]
        [ServiceFilter(typeof(UserAuthorisationFilter))]
        public ActionResult Logout()
        {
            User currentUser = _accountContextService.GetLoggedInUser();
            var successfulLogout = _accountService.SignOut(currentUser);
            if (successfulLogout)
                return Ok("User successfully logged out.");
            else
                return StatusCode(StatusCodes.Status500InternalServerError);
        }

        [HttpPost("deposit")]
        [ServiceFilter(typeof(UserAuthorisationFilter))]
        public ActionResult Deposit(Transaction transaction)
        {
            User currentUser = _accountContextService.GetLoggedInUser();
            var successfulDeposit = _accountService.DepositToAccount(currentUser, transaction.DollarAmountDepositedOrWithdrawn);
            if (successfulDeposit)
                return Ok($"You successfully deposited {transaction.DollarAmountDepositedOrWithdrawn} dollars.");
            else
                return StatusCode(StatusCodes.Status500InternalServerError);
        }

        [HttpPost("withdraw")]
        [ServiceFilter(typeof(UserAuthorisationFilter))]
        public ActionResult Withdraw(Transaction transaction)
        {
            User currentUser = _accountContextService.GetLoggedInUser();
            try
            {
                var successfulWithdrawal = _accountService.WithdrawFromAccount(currentUser, transaction.DollarAmountDepositedOrWithdrawn);
                if (successfulWithdrawal)
                    return Ok($"You successfully withdrew {transaction.DollarAmountDepositedOrWithdrawn} dollars.");
                else
                    return StatusCode(StatusCodes.Status500InternalServerError);
            }
            catch (AttemptingToWithdrawMoreThanAccountBalanceException e)
            {
                var errorList = new List<string>();
                errorList.Add("No money was withdrawn. Your account balance is less than your withdrawal amount request.");
                return BadRequest(new ErrorResponse(errorList));
            }
        }

        // POST: api/Account
        // You would lock this down, but for the sake of this example
        // we'll keep it open
        [AllowAnonymous]
        [HttpPost]
        public ActionResult Post([FromBody] User user)
        {
            var accountCreationResponse = _accountService.CreateAccount(user.Email, user.Password);
            switch (accountCreationResponse)
            {
                case Enum.AccountCreationFeedback.Success:
                    return Ok();
                case Enum.AccountCreationFeedback.AccountAlreadyExists:
                    return Conflict("User account already exists for this user.");
                default:
                    return Ok();
            }
        }

        [AllowAnonymous]
        [HttpPost("login")]
        public ActionResult Login([FromBody] User user)
        {
            var attemptLogin = _accountService.SignIn(user.Email, user.Password);
            if (attemptLogin.Item1 == true)
            {
                return Ok(attemptLogin.Item2);
            }
            else
            {
                return Unauthorized();
            }
        }
    }
}
