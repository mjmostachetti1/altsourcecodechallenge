Hello,

Here is my repo including: 
my asp.net project for implementing a bank ledger.
a Vue front-end app AltSourceVueFrontEnd directory.

You can run them both locally.
1) Open the solution in visual studio and run the AltSourceBankLedger projects w/ IIS Express.
2) Install node, run npm install within the front-end directory and then run 'node server.js' to serve the basic site
3) Visit localhost:8000

Additional info:
I supplied a postman json file since I enjoy using Postman to develop and test apis.
Just import the file into postman and you will see all the associated endpoints for testing when you run the project locally.
I also included a tests project for testing the account service a bit.

Best,
Mike